﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TDELFWebApplication.Models;

namespace TDELFWebApplication.Dados
{
    public class TDELFWebApplicationContext : DbContext
    {

        public TDELFWebApplicationContext(DbContextOptions<TDELFWebApplicationContext> options) : base(options)
        {
        }

        

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<ItemVenda> ItemVendas { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }



    }
}
