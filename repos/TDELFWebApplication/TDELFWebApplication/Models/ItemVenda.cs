﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TDELFWebApplication.Models
{
    public class ItemVenda
    {
        public int id { get; set; }
        public int idProduto { get; set; }
        public int quantidade { get; set; }
        public float valorTotal { get; set; }
        public float desconto { get; set; }
        
    }
}
