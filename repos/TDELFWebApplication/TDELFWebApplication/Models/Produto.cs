﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TDELFWebApplication.Models
{
    public class Produto
    {
        public int id { get; set; }
        public string nome { get; set; }
        public float preco { get; set; }
        public List<ItemVenda> ItensVendas { get; set; }
    }
}
